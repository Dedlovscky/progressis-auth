package ru.pavloffsoft.progressis.auth.providers;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;
import ru.pavloffsoft.progressis.auth.domain.users.SystemUser;
import ru.pavloffsoft.progressis.auth.security.SecurityConstants;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtProvider {
    public String getAccessToken(SystemUser systemUser){
        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(SecurityConstants.JWT_SECRET.getBytes()), SignatureAlgorithm.HS512)
                .setHeaderParam("type", SecurityConstants.TOKEN_TYPE)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.ACCESS_JWT_EXPIRATION_TIME))
                .setClaims(getClaims(systemUser))
                .compact();
    }

    public String getRefreshToken(SystemUser systemUser){
        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(SecurityConstants.JWT_SECRET.getBytes()), SignatureAlgorithm.HS512)
                .setHeaderParam("type", SecurityConstants.TOKEN_TYPE)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.REFRESH_JWT_EXPIRATION_TIME))
                .setClaims(getClaims(systemUser))
                .compact();
    }

    private Map<String, Object> getClaims(SystemUser systemUser){
        Map<String, Object> claims = new HashMap<>();

        claims.put("userId", systemUser.getUserId());
        claims.put("companyId", systemUser.getCompanyId());
        claims.put("employeeId", systemUser.getEmployeeId());
        claims.put("role", systemUser.getRole());
        claims.put("userGroupIds", systemUser.getUserGroupIds());

        return claims;
    }
}
