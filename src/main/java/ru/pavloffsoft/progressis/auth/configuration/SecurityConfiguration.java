package ru.pavloffsoft.progressis.auth.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import ru.pavloffsoft.progressis.auth.components.SystemAuthenticationEntryPoint;
import ru.pavloffsoft.progressis.auth.domain.users.types.Role;
import ru.pavloffsoft.progressis.auth.providers.JwtProvider;
import ru.pavloffsoft.progressis.auth.security.SecurityConstants;
import ru.pavloffsoft.progressis.auth.security.filters.JwtAuthenticationFilter;
import ru.pavloffsoft.progressis.auth.services.auth.UserDetailsServiceImp;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private SystemAuthenticationEntryPoint systemAuthenticationEntryPoint;
    @Autowired
    private UserDetailsServiceImp userDetailsService;
    @Autowired
    private JwtProvider jwtProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.anonymous().disable();
        http.cors().and()
                .formLogin().disable()
                .logout().disable()
                .csrf().disable()
                .httpBasic().disable()
//                .authorizeRequests()
//                    .antMatchers(SecurityConstants.SOURCES.get(Role.NONE)).permitAll()
//                    .antMatchers(SecurityConstants.SOURCES.get(Role.ARCHITECT)).hasRole(Role.ARCHITECT.name())
//                    .antMatchers(SecurityConstants.SOURCES.get(Role.ADMINISTRATOR)).hasRole(Role.ADMINISTRATOR.name())
//                .anyRequest().authenticated()
//                .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(systemAuthenticationEntryPoint)
                .and()
                    .addFilter(new JwtAuthenticationFilter(authenticationManager(), userDetailsService, jwtProvider))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());

        return source;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
