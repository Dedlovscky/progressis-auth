package ru.pavloffsoft.progressis.auth.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class Jdbc implements IJdbc {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void execute(String sql){
        jdbcTemplate.execute(sql);
    }

    public <TResult, TRowMapper extends RowMapper> TResult get(String sql, TRowMapper rowMapper){
        List<TResult> models = jdbcTemplate.query(sql, rowMapper);

        return models.size() == 0 ? null : models.get(0);
    }

    public <TResult, TRowMapper extends RowMapper> List<TResult> getList(String sql, TRowMapper rowMapper){
        return jdbcTemplate.query(sql, rowMapper);
    }
}
