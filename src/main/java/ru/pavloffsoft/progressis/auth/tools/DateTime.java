package ru.pavloffsoft.progressis.auth.tools;

import com.fasterxml.jackson.annotation.JsonValue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class DateTime {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("[yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS][yyyy-MM-dd'T'HH:mm:ss.SSSSSS][yyyy-MM-dd'T'HH:mm][yyyy-MM-dd HH:mm:ss][dd.MM.yyyy HH:mm:ss]");
    private static final DateTimeFormatter viewFormatter = DateTimeFormatter.ofPattern("[dd.MM.yyyy HH:mm:ss]");
    private static final DateTimeFormatter sqlFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private LocalDateTime localDateTime;

    public DateTime(String dateTime) {
        if(dateTime == null){
            localDateTime = null;
            return;
        }

        localDateTime = LocalDateTime.parse(dateTime, formatter);
    }

    public static DateTime NowUTC(){
        return new DateTime(LocalDateTime.parse(LocalDateTime.now(ZoneOffset.UTC).toString(), formatter).toString());
    }

    public DateTime plusHours(long value){
        localDateTime = localDateTime.plusHours(value);

        return new DateTime(localDateTime.toString());
    }

    public DateTime plusDays(long value){
        localDateTime = localDateTime.plusDays(value);

        return new DateTime(localDateTime.toString());
    }

    public DateTime minusHours(long value){
        localDateTime = localDateTime.minusHours(value);

        return new DateTime(localDateTime.toString());
    }

    public DateTime minusDays(long value){
        localDateTime = localDateTime.minusDays(value);

        return new DateTime(localDateTime.toString());
    }

    public DateTime startOfDay(){
        return new DateTime(localDateTime.of(localDateTime.toLocalDate(), LocalTime.MIN).toString());
    }

    public DateTime endOfDay(){
        return new DateTime(localDateTime.of(LocalDate.now(ZoneOffset.UTC), LocalTime.MAX).toString());
    }

    public String toSqlString(){
        if(localDateTime == null) return "NULL";
        return localDateTime.format(sqlFormatter);
    }

    @JsonValue
    @Override
    public String toString() {
        if(localDateTime == null) return null;

        return localDateTime.format(viewFormatter);
    }
}
