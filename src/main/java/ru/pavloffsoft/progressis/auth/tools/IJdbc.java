package ru.pavloffsoft.progressis.auth.tools;

import org.springframework.jdbc.core.RowMapper;

import java.util.List;

public interface IJdbc {
    void execute(String sql);

    <TResult, TRowMapper extends RowMapper> TResult get(String sql, TRowMapper rowMapper);
    <TResult, TRowMapper extends RowMapper> List<TResult> getList(String sql, TRowMapper rowMapper);
}
