package ru.pavloffsoft.progressis.auth.tools;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.UUID;

public class ID {
    private UUID id;

    public ID() { }

    public ID(String id) {
        this.id = UUID.fromString(id);
    }

    public ID(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public static ID NewId(){
        return new ID(UUID.randomUUID());
    }

    public static ID SystemId(){
        return new ID(UUID.fromString("00000000-0000-0000-0000-000000000000"));
    }

    public static ID ToId(String value){
        return new ID(convert(value));
    }

    public String toSqlString() {
        return "'" + id.toString() + "'";
    }

    private static UUID convert(String value){
        if(value == null || value.isBlank()) return null;

        UUID uuid;

        try {
            uuid = UUID.fromString(value);
        }catch (IllegalArgumentException e) {
            return null;
        }

        return uuid;
    }

    @JsonValue
    @Override
    public String toString() {
        if(id == null) return null;

        return id.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return  false;
        if(!(obj instanceof ID)) return false;

        return this.id.equals(((ID) obj).id);
    }
}
