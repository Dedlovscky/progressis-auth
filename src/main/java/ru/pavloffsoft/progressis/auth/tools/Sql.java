package ru.pavloffsoft.progressis.auth.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Sql {
    public static String read(String filePath) {
        File file = new File(System.getProperty("user.dir") + "/src/main/java/ru/pavloffsoft/progressis/auth/services" + filePath);

        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader reader = Files.newBufferedReader(file.toPath())) {
            String rowString;

            while ((rowString = reader.readLine()) != null) {//while there is content on the current line
                stringBuilder.append(rowString).append("\n"); // print the current line
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return stringBuilder.toString();
    }
}
