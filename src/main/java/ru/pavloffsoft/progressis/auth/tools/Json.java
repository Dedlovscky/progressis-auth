package ru.pavloffsoft.progressis.auth.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Json {
    public static <T> String stringify(T value){
        String jsonString = "";

        try {
            jsonString = new ObjectMapper().writeValueAsString(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonString;
    }

    public static <T> T parse(String jsonString){
        T data = null;

        try {
            data = new ObjectMapper().readValue(jsonString, new TypeReference<>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }
}
