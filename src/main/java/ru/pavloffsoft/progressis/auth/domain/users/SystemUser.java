package ru.pavloffsoft.progressis.auth.domain.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.pavloffsoft.progressis.auth.domain.users.types.Role;
import ru.pavloffsoft.progressis.auth.tools.ID;

import java.util.Collections;
import java.util.List;

public class SystemUser {
    private ID userId;
    private ID companyId;
    private ID employeeId;
    private Role role;
    private List<ID> userGroupIds;

    @JsonCreator
    public SystemUser(
            @JsonProperty("userId") ID userId,
            @JsonProperty("companyId") ID companyId,
            @JsonProperty("employeeId") ID employeeId,
            @JsonProperty("role") Role role,
            @JsonProperty("userGroupIds") List<ID> userGroupIds) {
        this.userId = userId;
        this.companyId = companyId;
        this.employeeId = employeeId;
        this.role = role;
        this.userGroupIds = userGroupIds;
    }

    public ID getUserId() {
        return userId;
    }

    public ID getCompanyId() {
        return companyId;
    }

    public ID getEmployeeId() {
        return employeeId;
    }

    public Role getRole() {
        return role;
    }

    public List<ID> getUserGroupIds() {
        return userGroupIds;
    }

    public List<GrantedAuthority> getGrantedAuthority(){
        List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(role.toString()));//new ArrayList<>(roles.size());

//        for (Role role : roles) {
//            authorities.add(new SimpleGrantedAuthority(role.toString()));
//        }
        return authorities;
    }
}
