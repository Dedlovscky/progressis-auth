package ru.pavloffsoft.progressis.auth.services.auth.converters;

import ru.pavloffsoft.progressis.auth.domain.users.SystemUser;
import ru.pavloffsoft.progressis.auth.services.auth.repositories.models.UserDB;

public final class UsersConverter {
    public static SystemUser Convert(UserDB userDB){
        return new SystemUser(userDB.getId(), userDB.getCompanyId(), userDB.getEmployeeId(), userDB.getRole(), userDB.getUserGroupIds());
    }
}
