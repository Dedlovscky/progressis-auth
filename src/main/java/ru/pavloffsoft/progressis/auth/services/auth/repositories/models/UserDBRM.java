package ru.pavloffsoft.progressis.auth.services.auth.repositories.models;

import org.springframework.jdbc.core.RowMapper;
import ru.pavloffsoft.progressis.auth.domain.users.types.Role;
import ru.pavloffsoft.progressis.auth.tools.DateTime;
import ru.pavloffsoft.progressis.auth.tools.ID;
import ru.pavloffsoft.progressis.auth.tools.Json;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDBRM implements RowMapper<UserDB> {
    @Override
    public UserDB mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserDB model = new UserDB();

        model.setId(ID.ToId(rs.getString("Id")));
        model.setCompanyId(ID.ToId(rs.getString("CompanyId")));
        model.setEmployeeId(ID.ToId(rs.getString("EmployeeId")));
        model.setUsername(rs.getString("Username"));
        model.setPassword(rs.getString("Password"));
        model.setRole(Role.getRole(rs.getInt("Role")));
        model.setUserGroupIds(Json.parse(rs.getString("UserGroupIds")));
        model.setCreatedUserId(ID.ToId(rs.getString("CreatedUserId")));
        model.setCreatedDateTimeUTC(new DateTime(rs.getString("CreatedDateTimeUTC")));
        model.setModifiedUserId(ID.ToId(rs.getString("ModifiedUserId")));
        model.setModifiedDateTimeUTC(new DateTime(rs.getString("ModifiedDateTimeUTC")));
        model.setRemoved(rs.getBoolean("IsRemoved"));

        return model;
    }
}
