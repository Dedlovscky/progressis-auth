package ru.pavloffsoft.progressis.auth.services.auth.repositories.models;

import ru.pavloffsoft.progressis.auth.domain.users.types.Role;
import ru.pavloffsoft.progressis.auth.tools.DateTime;
import ru.pavloffsoft.progressis.auth.tools.ID;

import java.util.List;

public class UserDB {
    private ID id;
    private ID companyId;
    private ID employeeId;
    private String username;
    private String password;
    private Role role;
    private List<ID> userGroupIds;

    private ID createdUserId;
    private DateTime createdDateTimeUTC;
    private ID modifiedUserId;
    private DateTime modifiedDateTimeUTC;
    private boolean isRemoved;

    public UserDB() { }

    public UserDB(ID id, ID companyId, ID employeeId, Role role, List<ID> userGroupIds, String username, String password,
                  ID createdUserId, DateTime createdDateTimeUTC, ID modifiedUserId, DateTime modifiedDateTimeUTC, boolean isRemoved) {
        this.id = id;
        this.companyId = companyId;
        this.employeeId = employeeId;
        this.role = role;
        this.userGroupIds = userGroupIds;
        this.username = username;
        this.password = password;
        this.createdUserId = createdUserId;
        this.createdDateTimeUTC = createdDateTimeUTC;
        this.modifiedUserId = modifiedUserId;
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
        this.isRemoved = isRemoved;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public ID getCompanyId() {
        return companyId;
    }

    public void setCompanyId(ID companyId) {
        this.companyId = companyId;
    }

    public ID getEmployeeId(){
        return employeeId;
    }

    public void setEmployeeId(ID employeeId){
        this.employeeId = employeeId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<ID> getUserGroupIds() {
        return userGroupIds;
    }

    public void setUserGroupIds(List<ID> userGroupIds) {
        this.userGroupIds = userGroupIds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ID getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(ID createdUserId) {
        this.createdUserId = createdUserId;
    }

    public DateTime getCreatedDateTimeUTC() {
        return createdDateTimeUTC;
    }

    public void setCreatedDateTimeUTC(DateTime createdDateTimeUTC) {
        this.createdDateTimeUTC = createdDateTimeUTC;
    }

    public ID getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(ID modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public DateTime getModifiedDateTimeUTC() {
        return modifiedDateTimeUTC;
    }

    public void setModifiedDateTimeUTC(DateTime modifiedDateTimeUTC) {
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }
}
