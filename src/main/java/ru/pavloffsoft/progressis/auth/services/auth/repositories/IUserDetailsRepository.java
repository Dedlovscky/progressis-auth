package ru.pavloffsoft.progressis.auth.services.auth.repositories;

import ru.pavloffsoft.progressis.auth.services.auth.repositories.models.UserDB;

public interface IUserDetailsRepository {
    UserDB getUser(String username);
}