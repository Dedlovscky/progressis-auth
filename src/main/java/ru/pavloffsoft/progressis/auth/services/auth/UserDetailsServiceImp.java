package ru.pavloffsoft.progressis.auth.services.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.pavloffsoft.progressis.auth.domain.users.SystemUser;
import ru.pavloffsoft.progressis.auth.services.auth.converters.UsersConverter;
import ru.pavloffsoft.progressis.auth.services.auth.repositories.IUserDetailsRepository;
import ru.pavloffsoft.progressis.auth.services.auth.repositories.models.UserDB;

@Service
public class UserDetailsServiceImp implements UserDetailsService {
    private SystemUser _system_user;

    @Autowired
    private IUserDetailsRepository userDetailsRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDB userDB = userDetailsRepository.getUser(username);

        if(userDB == null) throw new UsernameNotFoundException(username);

        _system_user = UsersConverter.Convert(userDB);

        return new User(userDB.getUsername(), userDB.getPassword(), _system_user.getGrantedAuthority());
    }

    public SystemUser getSystemUser(){
        return this._system_user;
    }
}
