package ru.pavloffsoft.progressis.auth.security;

import ru.pavloffsoft.progressis.auth.domain.users.types.Role;

import java.util.Map;

public final class SecurityConstants {
    public static final String AUTH_LOGIN_URL = "/api/auth";
    public static final String JWT_SECRET = "UGTuVBVMbbbSQvmsuvaEg9kNb8FDuRGqed3SjErVsbNhJ8TpTLzXdbdFhJQs9WkS";
    public static final long ACCESS_JWT_EXPIRATION_TIME = 2_880_000; //48 минут
    public static final long REFRESH_JWT_EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";

    /**
     * Список роли имеющие доступ к конкретным ресурсам
     * Role - ключ
     * String[] - массив URLs
     */
    public static final Map<Role, String[]> SOURCES = Map.ofEntries(
            Map.entry(Role.NONE, new String[] {}),
            Map.entry(Role.ARCHITECT, new String[] {
                    "/**"
            }),
            Map.entry(Role.ADMINISTRATOR, new String[] {
                    "/api/v1/menu/get",
                    "/api/v1/employees/remove",
                    "/api/v1/employees/getEmployees"
            })
    );
}
