package ru.pavloffsoft.progressis.auth.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.pavloffsoft.progressis.auth.providers.JwtProvider;
import ru.pavloffsoft.progressis.auth.security.SecurityConstants;
import ru.pavloffsoft.progressis.auth.services.auth.UserDetailsServiceImp;
import ru.pavloffsoft.progressis.auth.tools.AuthenticationResponse;
import ru.pavloffsoft.progressis.auth.tools.AuthorizationRequest;
import ru.pavloffsoft.progressis.auth.tools.Response;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Фильтр аутентификации пользователя
 */
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImp userDetailsService;
    private final JwtProvider jwtProvider;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, UserDetailsServiceImp userDetailsService, JwtProvider jwtProvider) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtProvider = jwtProvider;

        setFilterProcessesUrl(SecurityConstants.AUTH_LOGIN_URL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            AuthorizationRequest authorization = new ObjectMapper().readValue(request.getInputStream(), AuthorizationRequest.class);

            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(authorization.getUsername(), authorization.getPassword());

            return authenticationManager.authenticate(authenticationToken);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain,
                                            Authentication authentication) throws IOException {

        AuthenticationResponse authenticationResponse = new AuthenticationResponse(
                jwtProvider.getAccessToken(userDetailsService.getSystemUser()), jwtProvider.getRefreshToken(userDetailsService.getSystemUser()));

        String json = new ObjectMapper().writeValueAsString(new Response<>(authenticationResponse));

        response.getWriter().write(json);
    }
}
